# Live Project -- AWS Functions

The full [Project Series](https://liveproject.manning.com/series/2274)

## Useful links

[Live Project #1](https://liveproject.manning.com/module/445)

[Live Project #2](https://liveproject.manning.com/project/446)

[Live Project #3](https://liveproject.manning.com/project/447)

[Live Project #4](https://liveproject.manning.com/project/459)


[API Endpoint](https://o308uusrpk.execute-api.us-west-2.amazonaws.com/Prod)

[AWS Lambda in GO](https://docs.aws.amazon.com/lambda/latest/dg/lambda-golang.html)

[Deploy API for Lambda](https://medium.com/softkraft/aws-lambda-in-golang-the-ultimate-guide-4eec6c53149a)


# Project #1: Deploying & Invoking Lambda Functions

After deploying the backend Function, test it works with:

```shell
export API="https://o308uusrpk.execute-api.{{ AWS Region }}.amazonaws.com/Prod"

http POST ${API}/hotel \
    buyer_id=234 \
    start_date="2021-06-30" \
    end_date="2021-07-10" \
    near="Vatican Museums"
```
Response:

```json
{
    "end_date": "2021-07-10",
    "name": "R3",
    "reservation_id": "GSDJN",
    "start_date": "2021-06-30"
}
```


## Deploying to Lambda

**Building the ZIP archive**:

```shell
GOOS=linux go build -o book-hotel book_hotel.go && \
    zip book-hotel.zip book-hotel
```

**Creating an IAM Role**:

Create a JSON policy file (`trust-policy.json`) to allow the Lambda function to assume a `Role` (this will be needed so that it can assume the `AWSLambdaBasicExecutionRole` -- see further below):

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```

then create the role in IAM:

```shell
aws iam create-role --role-name LambdaBasicExecutionRole \
    --assume-role-policy-document file://lambda-trust-policy.json
```

note the `ARN` for the created role:

```shell
aws iam get-role --role-name LambdaBasicExecutionRole
```
```json
 {
     "Role": {
         "Path": "/",
         "RoleName": "LambdaBasicExecutionRole",
         "RoleId": "AROA6QQTFLQ2YKEIQSDFZ",
>>>>     "Arn": "arn:aws:iam::<<ACCOUNT>>:role/LambdaBasicExecutionRole",
         "CreateDate": "2021-07-15T05:02:35+00:00",
         "AssumeRolePolicyDocument": {
             "Version": "2012-10-17",
             "Statement": [
                 {
                     "Effect": "Allow",
                     "Principal": {
                         "Service": "lambda.amazonaws.com"
                     },
                     "Action": "sts:AssumeRole"
                 }
             ]
         },
         "MaxSessionDuration": 3600,
         "RoleLastUsed": {}
     }
 }
```

Finally, attach the `AWSLambdaBasicExecutionRole` policy so that our function can create a log group/stream and write logs to it:

```shell
aws iam attach-role-policy --role-name LambdaBasicExecutionRole \
    --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```

The logs will be available at the following URL:

```
https://{{ AWS Region }}.console.aws.amazon.com/cloudwatch/home?region={{ AWS Region }}#logsV2:log-groups/log-group/$252Faws$252Flambda$252Fbook-hotel
```


**Deploying to AWS**:

Note the `"handler"` is the name of the **executable** that was created during the `go build` step (`-o`);
and `fileb://` is actually the correct Scheme, not a typo:

```shell
aws lambda create-function --function-name book-hotel \
    --zip-file fileb://book-hotel.zip \
    --role arn:aws:iam::{{ AWS Account }}:role/LambdaBasicExecutionRole \
    --runtime go1.x \
    --handler book-hotel
```

## Invoking the Lambda

First we need to create an API endpoint via the [API Gateway service](https://{{ AWS Region }}.console.aws.amazon.com/apigateway/home?region={{ AWS Region }})
and create a `Resource` (`book-hotel`) and a `Method` (`POST`) to invoke using the `Actions` drop-down:

![API Gateway](docs/images/api-resource.png)

Then `Deploy` the API (again with the `Actions`), this creates a `Stage` (called `Staging` in the example below):

![Staging API](docs/images/stage.png)

from which we derive the invocation URL:

```shell
export BOOKINGS_API="https://o35tsi3jvc.execute-api.{{ AWS Region }}.amazonaws.com/Staging"

http POST $BOOKINGS_API/hotel \
    buyer_id="X5A-234-0" \
    start_date="2021-06-30" \
    end_date="2021-07-10" \
    near="Vatican Museums"
```
```
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 88
Content-Type: application/json
Date: Thu, 15 Jul 2021 06:39:26 GMT
X-Amzn-Trace-Id: Root=1-60efd81e-5e9d007b56bd72cf1e641afb;Sampled=0
x-amz-apigw-id: Cf60wEgIPHcF00Q=
x-amzn-RequestId: 9eace3c0-5edb-4175-ad8b-36cf9ef01c9a

{
    "end_date": "2021-07-10",
    "name": "R2",
    "reservation_id": "TIAQI",
    "start_date": "2021-06-30"
}
```

Similarly, to book a museum, we use the `/museum` endpoint:

```shell
http POST $BOOKINGS_API/museum \
    buyer_id="X5A-234-0" \
    when="2021-06-15" \
    museum_name="Vatican Museums"
```
```
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 100
Content-Type: application/json
Date: Tue, 20 Jul 2021 05:02:03 GMT
X-Amzn-Trace-Id: Root=1-60f658cb-3116a36a3aa6d25061f4e28a;Sampled=0
x-amz-apigw-id: CwLP1EqhPHcFX-g=
x-amzn-RequestId: f0a25b80-93de-47da-a5fc-afb726072789

{
    "error_code": 0,
    "errors": null,
    "name": "Vatican Museums",
    "reservation_id": "BYAPN",
    "when": "2021-06-15"
}

```

# Project #2: Using Step Functions

*Create the StepFunctionsBasicRole*

This is needed so that the StepFunction can execute and log execution details to CloudWatch:

```shell

aws iam create-policy --policy-name LogsAccessPolicy \
    --policy-document file://logs-trust-policy.json

aws iam create-role --role-name StepFunctionsBasicRole \
    --assume-role-policy-document file://stepfunc-trust-policy.json

aws iam attach-role-policy --role-name StepFunctionsBasicRole \
    --policy-arn arn:aws:iam::{{ AWS Account }}:policy/LogsAccessPolicy
```

`StepFunctionsBasicRole` is the `Role` that will have to be chosen in the Console after designing the Step Function.


# Project #4 - Use SNS to orchestrate services

## Create the SNS Topic and Enable the Step Function to Publish to it

Create a SNS topic:

```shell
TOPIC=OrderCompletedTopic
aws sns create-topic --name ${TOPIC}
{
    "TopicArn": "arn:aws:sns:{{ AWS Region }}:{{ AWS Account }}:OrderCompletedTopic"
}
```

Replace the `"TopicArn"` in the `sns-publish-policy.json` policy:

```shell
POLICY=sns-publish-policy.json
sed  "s/{{ .TopicArn }}/${TOPIC_ARN}/" policies/${POLICY} > /tmp/${POLICY}

```

then create this policy and attach it to the `Role` used by the Step Function (`StepFunctionsBasicRole`):

```shell
POLICY_ARN=$(aws iam create-policy --policy-name SnsPublishPolicy-${TOPIC} \
    --policy-document file:///tmp/${POLICY} | jq -r ".Arn"

aws iam attach-role-policy --role-name StepFunctionsBasicRole \
    --policy-arn ${POLICY_ARN}
```

**NOTE**
> If the `SnsPublishPolicy-${TOPIC}` already exists, find its ARN via:<br>
> ```aws iam list-policies | jq ".Policies[].Arn" | grep SnsPublishPolicy```

The `create-sns-topic` script automates the process, use it like this:

```shell
$ ./create-sns-topic.sh OrderCompleteTopic StepFunctionBasicRole
```
### Milestone 2 -- Save Invoice to S3

**Create an S3 bucket**
First step is to create an S3 bucket `BookingInvoices`:

```shell
aws s3 mb s3://orderworkflow
```


**Create the Role**
The `generate-invoice` function needs to have S3 access permissions, so we create a specific Role for it and we attach the `S3FullAccess` role:


```shell
./create-lambda-role.sh LambdaWithS3AccessRole

aws iam attach-role-policy --role-name LambdaWithS3AccessRole \
--policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess 


aws lambda create-function --function-name generate-invoice \
  --zip-file fileb://build/generate-invoice.zip \
  --role arn:aws:iam::997546351669:role/LambdaWithS3AccessRole \
  --runtime go1.x \
  --handler generate-invoice

```

**Parse the incoming SNS Message**
The format of the SNS message is a bit gnarly and carries the actual JSON encoded as an "escaped string":

```json
{
    "Records": [
        {
            "Sns": {
                "Message": "{\"purchase\":{\"buyer_id\":\"mariano3\"},\"hotel\":{\"start_date\":\"2020-03-13\",\"end_date\":\"2020-03-15\",\"reservation_id\":\"XULOQ\",\"name\":\"L1\"},\"museum\":{\"when\":\"2020-03-14\",\"reservation_id\":\"YOZYC\",\"name\":\"tate gallery\"}}"
            }
        }
    ]
}

```
We define this as a `SnsRecord` type, so that it can marshaled by the runtime:

```go
// See invoices.go

package bookings

type SnsMessage struct {
	Message string
}

type Record struct {
	Sns SnsMessage
}

type SnsRecords struct {
	Records []Record
}
```

# Annex - How to delete a Role

First, find all `Role`s that have been already defined:

```shell
$ aws iam list-roles | jq ".Roles[].RoleName"

"AWSDeepLensGreengrassGroupRole"
...
"StepFunctions-MyStateMachine-role-bdad2ca6"
"StepFunctionsBasicRole"
```

If we want to remove `StepFunctions-MyStateMachine-role-bdad2ca6`, we first need to detach the attached policies, or we will get an error:

```
$ aws iam delete-role --role-name StepFunctions-MyStateMachine-role-bdad2ca6

An error occurred (DeleteConflict) when calling the DeleteRole operation: Cannot delete entity, must detach all policies first.
```

to do this, we need to find their ARNs first, and then detach them one by one, using `aws iam detach-role-policy`:

```
$ aws iam list-attached-role-policies --role-name StepFunctions-MyStateMachine-role-bdad2ca6

{
    "AttachedPolicies": [
        {
            "PolicyName": "LambdaInvokeScopedAccessPolicy-1e6c737f-1167-40d8-ba69-75ac8b60cc07",
            "PolicyArn": "arn:aws:iam::99...0cc07"
        },
        ...
    ]
}

$ aws iam detach-role-policy \
    --role-name StepFunctions-MyStateMachine-role-bdad2ca6 \
    --policy-arn "arn:aws:iam::99...0cc07"
    
$ aws iam delete-role --role-name StepFunctions-MyStateMachine-role-bdad2ca6
```

All this can be automated: see [this gist](https://gist.github.com/massenz/88483c6ce78e0bf212aa6c928478af9e)
