#!/usr/bin/env bash

set -eu
declare -r BUILDDIR=build

# First test that this all works at a pretty basic level
# TODO: need real unit tests.
echo "Running tests"
go run test_funcs.go > /dev/null

EVERYTHING=(book-hotel book-museum book-cancel generate-invoice)

if [[ $# == 0 ]]; then
  TARGETS=${EVERYTHING[*]}
else
  TARGETS=$@
fi

echo "Building and Uploading: ${TARGETS}"
for what in ${TARGETS[*]}; do
  ENTRYPOINT=${what}
  BIN=${BUILDDIR}/${ENTRYPOINT}
  ZIPFILE=${BUILDDIR}/${ENTRYPOINT}.zip

  echo "Building ${BIN}"
  GOOS=linux go build -o ${BIN} ${ENTRYPOINT}.go && \
      zip -jv ${ZIPFILE} ${BIN}

  echo "Uploading ${ENTRYPOINT} to AWS Lambda"
  aws lambda update-function-code --function-name ${ENTRYPOINT} \
      --zip-file fileb://${ZIPFILE} | jq ".FunctionArn, .LastUpdateStatus"
done
echo "SUCCESS: done"
