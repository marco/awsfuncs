#!/usr/bin/env bash
#
# Usage: create-sns-topic TOPIC [ROLE]
#
# Creates an SNS TOPIC, then attaches a policy to
# publish to this topic to the given ROLE, if specified.

set -eux

declare -r TOPIC=${1:-}
declare -r ROLE=${2:-}
declare -r POLICY="sns-publish-policy.json"

if [[ -z ${TOPIC} ]]; then
  echo "Usage: create-sns-topic TOPIC [ROLE]"
  echo "ERROR: TOPIC must be specified"
  exit 1
fi

TOPIC_ARN=$(aws sns create-topic --name ${TOPIC} | jq -r ".TopicArn")
echo "SUCCESS: Topic ${TOPIC} created successfully: ${TOPIC_ARN}"

if [[ -n ${ROLE} ]]; then
  sed  "s/{{ .TopicArn }}/${TOPIC_ARN}/" policies/${POLICY} > /tmp/${POLICY}

  POLICY_ARN=$(aws iam create-policy --policy-name SnsPublishPolicy-${TOPIC} \
    --policy-document file:///tmp/${POLICY} | jq -r ".Policy.Arn")

  if [[ -z ${POLICY_ARN} ]]; then
    echo "ERROR: could not create policy"
    exit 1
  fi

  aws iam attach-role-policy --role-name ${ROLE} \
    --policy-arn "${POLICY_ARN}"
  echo "SUCCESS: Policy ${POLICY_ARN} attached to Role ${ROLE}"
fi
