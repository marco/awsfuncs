package main

import (
	"bitbucket.org/marco/awsfuncs/bookings"
	"context"
	"fmt"
)

func main() {
	r2 := bookings.HotelBookingRequest{
		Buyer:     "Joe Bid",
		StartDate: "2021-05-31",
		EndDate:   "2021-06-22",
		Near:      "Tate Gallery",
	}

	// Using `Background` which is an empty context, specifically defined for testing.
	response2, err := bookings.HandleHotelBookingRequest(context.Background(), r2)

	if err != nil {
		fmt.Printf("ERROR: %v\n", err)
		return
	} else {
		fmt.Println(response2)
	}

	r1 := bookings.MuseumBookingRequest{
		Buyer:      "Joe Bid",
		When:       "2021-05-31",
		MuseumName: "Tate Gallery",
	}

	response, err := bookings.HandleMuseumBookingRequest(context.Background(), r1)
	if err != nil {
		fmt.Printf("ERROR: %v\n", err)
		cancelHotelBooking(response2.ReservationId)
	} else {
		fmt.Println(response)
	}
}

func cancelHotelBooking(reservationId string) {
	r3 := bookings.CancelBookingRequest{
		ReservationId: reservationId,
	}

	if err := bookings.CancelHotelBooking(r3); err != nil {
		fmt.Printf("ERROR: %v\n", err)
	} else {
		fmt.Printf("SUCCESS: Reservation %s canceled\n", reservationId)
	}
}
