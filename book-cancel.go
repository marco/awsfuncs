package main

import (
	"bitbucket.org/marco/awsfuncs/bookings"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(bookings.CancelHotelBooking)
}
