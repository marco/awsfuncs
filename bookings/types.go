package bookings

const (
	GatewayApi       = "https://o308uusrpk.execute-api.us-west-2.amazonaws.com/Prod"
	HotelBookingUrl  = GatewayApi + "/hotel"
	MuseumBookingUrl = GatewayApi + "/museum"

	S3Bucket = "orderworkflow"
)

type HotelBookingRequest struct {
	Buyer     string `json:"buyer_id"`
	StartDate string `json:"start_date"`
	EndDate   string `json:"end_date"`
	Near      string `json:"near"`
}

type MuseumBookingRequest struct {
	Buyer      string `json:"buyer_id"`
	When       string `json:"when"`
	MuseumName string `json:"museum_name"`
}

type CancelBookingRequest struct {
	ReservationId string `json:"reservation_id"`
}

type HotelBookingResponse struct {
	ReservationId string `json:"reservation_id"`
	Name          string `json:"name"`
	StartDate     string `json:"start_date"`
	EndDate       string `json:"end_date"`
}

type MuseumBookingResponse struct {
	ReservationId string `json:"reservation_id"`
	Name          string `json:"name"`
	When          string `json:"when"`
}

type Purchase struct {
	Buyer string `json:"buyer_id"`
}

type PurchaseRecord struct {
	Purchase      Purchase              `json:"purchase"`
	HotelBooking  HotelBookingResponse  `json:"hotel"`
	MuseumBooking MuseumBookingResponse `json:"museum"`
}
