package bookings

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

func ProcessResponse(serverResponse []byte, statusCode int, confirmation interface{}) error {
	var err error
	if statusCode == http.StatusOK {
		err = json.Unmarshal(serverResponse, &confirmation)
		if err != nil {
			log.Printf("ERROR: %s", err.Error())
		}
		return err
	} else {
		if statusCode == http.StatusServiceUnavailable {
			return &TransientError{}
		} else {
			var errorResponse HandlerError
			err = json.Unmarshal(serverResponse, &errorResponse)
			if err != nil {
				log.Printf("ERROR: Decoding response %s (%s)",
					string(serverResponse), err.Error())
				return err
			}
			return errorResponse
		}
	}
}

// delete sends a DELETE request to the URL specified
func httpDelete(url string, body []byte) (err error) {

	// Create client
	client := &http.Client{}

	// Create request
	req, err := http.NewRequest(http.MethodDelete, url,
		bytes.NewBuffer(body))
	if err != nil {
		log.Printf("ERROR: %s", err.Error())
		return
	}

	// Fetch Request
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("ERROR: %s", err.Error())
		return
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		log.Printf("ERROR: %s", err.Error())
		return
	}
	// The response for the DELETE, if successful is always empty, so it can be safely ignored,
	// but it needs something to unmarshal it into.
	var ignore map[string]string
	return ProcessResponse(respBody, resp.StatusCode, &ignore)
}
