package bookings

import (
	"strings"
)

// TransientError is a "marker" error type to indicate the upstream server
// returned a 503 error and the request should be retried.
type TransientError struct {
}

func (e TransientError) Error() string {
	return "transient error"
}

type HandlerError struct {
	Errors    []string `json:"errors"`
}

func (e HandlerError) Error() string {
	return strings.Join(e.Errors, ", ")
}

func NewHandlerError(err error) HandlerError {
	return HandlerError{Errors: []string{err.Error()}}
}
