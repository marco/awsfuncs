package bookings

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)


func HandleMuseumBookingRequest(ctx context.Context, booking MuseumBookingRequest) (
	confirmation MuseumBookingResponse, err error) {

	log.Printf("Executing Booking request: %s\n", booking)
	request, _ := json.Marshal(booking)

	response, err := http.Post(MuseumBookingUrl, "application/json",
		bytes.NewBuffer(request))

	if err != nil {
		log.Print(err)
		return
	}

	var responseBody []byte
	responseBody, err = ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		log.Print(err)
		return
	}
	log.Printf("[%d] %v", response.StatusCode, string(responseBody))

	err = json.Unmarshal(responseBody, &confirmation)
	if err != nil {
		log.Print(err)
		return
	}
	err = ProcessResponse(responseBody, response.StatusCode, &confirmation)
	return
}
