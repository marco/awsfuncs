package bookings

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"log"
)

type SnsMessage struct {
	Message string
}

type Record struct {
	Sns SnsMessage
}

type SnsRecords struct {
	Records []Record
}


func StoreInvoicesHandler(message SnsRecords) error {
	for _, record := range message.Records {
		msg := record.Sns.Message
		// The message is actually JSON, but is formatted as a string, so we
		// need to effect a double conversion.
		var purchase PurchaseRecord
		if err := json.Unmarshal([]byte(msg), &purchase); err != nil {
			return NewHandlerError(err)
		}
		log.Printf("Generating invoice for %s (%s, %s)", purchase.Purchase.Buyer,
			purchase.HotelBooking.ReservationId, purchase.MuseumBooking.ReservationId)
		if err := writeInvoice(purchase); err != nil {
			return NewHandlerError(err)
		}
	}
	return nil
}

func writeInvoice(invoice PurchaseRecord) (err error) {

	// The session the S3 Uploader will use
	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	}))

	// Create an uploader with the session and default options
	uploader := s3manager.NewUploader(sess)

	// Generate the file contents.
	body, err := json.MarshalIndent(invoice, "", "    ")
	if err != nil {
		return err
	}

	// Upload the file to S3.
	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(S3Bucket),
		// TODO: A better naming strategy
		Key:    aws.String(invoice.HotelBooking.ReservationId + ".json"),
		Body:   bytes.NewBuffer(body),
	})
	if err != nil {
		return err
	}
	fmt.Printf("file uploaded to, %s\n", aws.StringValue(&result.Location))
	return nil
}
