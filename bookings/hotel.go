package bookings

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

func HandleHotelBookingRequest(ctx context.Context, booking HotelBookingRequest) (
	confirmation HotelBookingResponse, err error) {

	log.Printf("Executing Booking request: %s\n", booking)
	request, _ := json.Marshal(booking)

	response, err := http.Post(HotelBookingUrl, "application/json",
		bytes.NewBuffer(request))

	if err != nil {
		log.Print(err)
		return
	}

	var responseBody []byte
	responseBody, err = ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		log.Print(err)
		return
	}
	log.Printf("[%d] %v", response.StatusCode, string(responseBody))
	err = ProcessResponse(responseBody, response.StatusCode, &confirmation)
	return
}

func CancelHotelBooking(booking CancelBookingRequest) error {

	log.Printf("Cancelling Booking request: %s\n", booking.ReservationId)
	request, _ := json.Marshal(booking)

	err := httpDelete(HotelBookingUrl, request)
	if err != nil {
		log.Printf("ERROR: %s", err)
	} else {
		log.Printf("SUCCESS: Reservation %s canceled", booking.ReservationId)
	}
	return err
}
