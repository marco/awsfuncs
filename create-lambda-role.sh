#!/usr/bin/env bash
#
# Usage: create-lambda-role ROLE
#
# Creates a ROLE that can be used for Lambda Functions, enables basic permissions.

set -eux

declare -r ROLE=${1:-}
declare -r POLICY="LambdaTrustPolicy"

if [[ -z ${ROLE} ]]; then
  echo "Usage: create-lambda-role ROLE"
  echo "ERROR: ROLE must be specified"
  exit 1
fi

aws iam create-role --role-name ${ROLE} \
    --assume-role-policy-document file://policies/lambda-trust-policy.json

aws iam attach-role-policy --role-name ${ROLE} \
    --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

echo "SUCCESS: Role ${ROLE} created"
